using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    public int pointValue;

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            transform.Rotate(0, 2, 0);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.GemUpdateScore(pointValue);
        }
    }
}