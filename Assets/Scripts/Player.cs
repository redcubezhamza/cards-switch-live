using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player: MonoBehaviour
{
    public Rigidbody rb;
    private float force = 8f;

    private Touch touch;
    public float sensitivity = 0.005f;

    private float xRange = 1.5f;

    public bool gameOver = false;
    private bool isInvincible = false;

    private MeshRenderer mr;
    public Material nextCardMaterial;
    //public MeshFilter mf;

    public string currentMaterial;
    [SerializeField] float blinkingDelay = 0.2f;

    public Material heart01;
    public Material heart02;
    public Material heart03;
    public Material heart04;
    public Material heart05;
    public Material heart06;
    public Material heart07;
    public Material heart08;
    public Material heart09;
    public Material heart10;
    public Material king;
    public Material queen;
    public Material jack;

    public static Player Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        // SetRandomObject();

        mr = GetComponent<MeshRenderer>();

       // InvokeRepeating("ShapeChange", 0, 07);
        InvokeRepeating("Blink", 05, 07);

        Invoke("ShapeChange", 1.5f);

        
    }

    void Update()
    {
        MoveForward();
        XAxisRange();
    }

    void FixedUpdate()
    {
        //Touch touch = Input.GetTouch(0);

        //if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        //{
        //    if (touch.position.x > Screen.width / 2)
        //    {
        //        StartCoroutine(MovingR());
        //        IEnumerator MovingR()
        //        {
        //            transform.position += new Vector3(1, 0, 0);
        //            isMoved = true;

        //            yield return new WaitForSeconds(0.25f);
                    

        //        }
                
                
        //        //transform.Translate(moveUnit, 0, 0);
        //    }
        //    //else if (touch.position.x < Screen.width / 2)
        //    else
        //    {

        //        StartCoroutine(MovingL());
        //        IEnumerator MovingL()
        //        {
                    
        //            transform.position += new Vector3(-1, 0, 0);
        //            isMoved = true;

        //            yield return new WaitForSeconds(0.25f);
                    

        //        }
        //        //transform.Translate(-moveUnit, 0, 0);
        //    }
        //}

    }

    public void TouchRight()
    {
        transform.Translate(1f, 0, 0);
    }

    public void TouchLeft()
    {
        transform.Translate(-1f, 0, 0);
    }

    void MoveForward()
    {
        rb.velocity = Vector3.forward * force;
        if (GameManager.Instance.timeValue >= 15)
        {
            rb.velocity = Vector3.forward * (force + 4);
        }

        if (GameManager.Instance.timeValue >= 30)
        {
            rb.velocity = Vector3.forward * (force + 8);
        }

        if (GameManager.Instance.timeValue >= 45)
        {
            rb.velocity = Vector3.forward * (force + 12);
        }

        if (GameManager.Instance.timeValue >= 60)
        {
            rb.velocity = Vector3.forward * (force + 16);
        }

        if (GameManager.Instance.timeValue >= 75)
        {
            rb.velocity = Vector3.forward * (force + 20);
        }

        //if (Time.time > initialTime)
        //{
        //    //while(true) 
        //    {
        //        initialTime += 5;
        //        force += 4;
        //        rb.velocity = Vector3.forward * force;
        //        Debug.Log("B " + force);
        //    }
        //}
        //else
        //{
        //    rb.velocity = Vector3.forward * force;
        //    Debug.Log("A " + force);
        //}
    }

    void Blink()
    {
        if (!gameOver)
        {
            StartCoroutine(Blink());
            IEnumerator Blink()
            {
                isInvincible = true;
                {
                    mr.enabled = false;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = true;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = false;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = true;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = false;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = true;
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = false;
                    SetRandomObject();
                    yield return new WaitForSeconds(blinkingDelay);
                    mr.enabled = true;

                }
                isInvincible = false;


            }

        }
        
    }
    
    void SetRandomObject()
    {
        //int index = Random.Range(0, 1);
      //  int index = Random.Range(0, 13);

        mr.material = nextCardMaterial;
        currentMaterial = nextCardMaterial.name;
      

        //switch (index)
        //{
        //    case 0:
        //        //currentMaterial = "Cyan";
        //        //mr.material = cyan;
        //        //mf.mesh = sphere;
        //        currentMaterial = "14";
        //        mr.material = heart01;
        //        break;
        //    case 1:
        //        //currentMaterial = "Greenish";
        //        //mr.material = greenish;
        //        //mf.mesh = cylinder;
        //        currentMaterial = "2";
        //        mr.material = heart02;
        //        break;
        //    case 2:
        //        //currentMaterial = "Magenta";
        //        //mr.material = magenta;
        //        //mf.mesh = cube;
        //        currentMaterial = "3";
        //        mr.material = heart03;
        //        break;
        //    case 3:
        //        //currentMaterial = "Pink";
        //        //mr.material = pink;
        //        //mf.mesh = capsule;
        //        currentMaterial = "4";
        //        mr.material = heart04;
        //        break;
        //    case 4:
        //        currentMaterial = "5";
        //        mr.material = heart05;
        //        break;
        //    case 5:
        //        currentMaterial = "6";
        //        mr.material = heart06;
        //        break;
        //    case 6:
        //        currentMaterial = "7";
        //        mr.material = heart07;
        //        break;
        //    case 7:
        //        currentMaterial = "8";
        //        mr.material = heart08;
        //        break;
        //    case 8:
        //        currentMaterial = "9";
        //        mr.material = heart09;
        //        break;
        //    case 9:
        //        currentMaterial = "10";
        //        mr.material = heart10;
        //        break;
        //    case 10:
        //        currentMaterial = "13";
        //        mr.material = king;
        //        break;
        //    case 11:
        //        currentMaterial = "12";
        //        mr.material = queen;
        //        break;
        //    case 12:
        //        currentMaterial = "11";
        //        mr.material = jack;
        //        break;
       // }
    }
    
    void XAxisRange()
    {
        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        {
            Debug.Log("<color=blue>Collided card tag is </color>" + other.tag);
            if (other.tag == currentMaterial)
            {
                GameManager.Instance.score++;
                Destroy(other.gameObject);
            }
            else
            {
                Debug.Log("other card tag " + other.gameObject.tag + "current material name" + currentMaterial);

                if (other.CompareTag("Gem"))
                {
                    GameManager.Instance.coinCollectSound.Play();
                    Destroy(other.gameObject);
                }
                else
                {
                    if(isInvincible == true)
                    {
                        Destroy(other.gameObject);
                    }
                    else
                    {
                        gameOver = true;
                        Destroy(other.gameObject);
                        GameManager.Instance.PlayerCollidewithAnotherCard();
                    }
                }
            }
        }
    }

    void ShapeChange()
    {
        SetRandomObject();
    }
}
