using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    private bool isPaused;
    private bool isShareButtonClicked;
    private bool isShopButtonClicked;

    public GameObject pauseButton;
    public GameObject playButton;
    public GameObject pauseImage;
    public GameObject playImage;
    public GameObject pausePanel;
    public GameObject gameOverPanel;
    public GameObject player;
    public GameObject networkErrorPanel;

    public AudioSource levelMusic;
    public AudioSource pauseSound;
    public AudioSource clickSound;
    public AudioSource coinCollectSound;

    public int gemScore;
    public int currentGem;
    public int gem;

    public int score = 0;

    public GameObject scoreDisplay;
    public GameObject scoreDisplayatGameOver;

    public float timeValue;
    public GameObject timerText;

    [SerializeField] GameObject scoreSharePanel;
    [SerializeField] Text scoreDisplayAtSharePanel;
    [SerializeField] Text dateText;

    public GameObject leaderboardPanel;
    public GameObject nameWindow;

    public static GameManager Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        PlayerPrefs.SetInt("NameWindow", 0);

        gemScore = PlayerPrefs.GetInt("Gems");
        InterstitialAds.Instance.RequestInterstitial();

        //StartCoroutine(CheckEvery15Seconds());
    }

    void Update()
    {
        Score();
        ScoreDisplayatGameOver();
        DisplayTime(timeValue);

        //UpdateLeaderboardOnObstacleCollision();

        currentGem = gem + gemScore;
        PlayerPrefs.SetInt("Gems", currentGem);
        PlayerPrefs.Save();       
    }

    //IEnumerator CheckEvery15Seconds()
    //{
    //    yield return new WaitForSeconds(03);
    //    StartCoroutine(CheckNetworkConnection());
    //}

    //IEnumerator CheckNetworkConnection()
    //{
    //    UnityWebRequest request = new UnityWebRequest("https://www.google.com");
    //    yield return request.SendWebRequest();

    //    if (request.error != null)
    //    {
    //        networkErrorPanel.SetActive(true);
    //        levelMusic.Pause();
    //        Player.Instance.gameOver = true;
    //        player.SetActive(false);
    //    }
    //    else
    //    {
    //        networkErrorPanel.SetActive(false);
    //        levelMusic.UnPause();
    //        Player.Instance.gameOver = false;
    //        player.SetActive(true);
    //    }
    //}

    public void UpdateLeaderboardOnObstacleCollision()
    {
        //if (Player.Instance.collideWithObstacle == true)
        {
            StartCoroutine(Collidewithobstacle());
            IEnumerator Collidewithobstacle()
            {
                yield return new WaitForSeconds(5);
                PlayfabManager.instance.SendLeaderboard(score);
                //Player.Instance.collideWithObstacle = false;
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        if (Player.Instance.gameOver == false)
        {
            timeValue += Time.deltaTime;
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            timerText.GetComponent<Text>().text = string.Format("{0:00}", seconds);
        }
    }

    public void GemUpdateScore(int gemToAdd)
    {
        gem += gemToAdd;
    }

    public void Score()
    {
        scoreDisplay.GetComponent<Text>().text = score.ToString();

        if (score < 0)
        {
            scoreDisplay.GetComponent<Text>().text = "0" + 0.ToString();
        }
        else
        {
            if (score < 10)
            {
                scoreDisplay.GetComponent<Text>().text = "0" + score;
            }
            else
            {
                scoreDisplay.GetComponent<Text>().text = score.ToString();
            }
        }
    }

    void ScoreDisplayatGameOver()
    {
        scoreDisplayatGameOver.GetComponent<Text>().text = score.ToString();
        if (score < 0)
        {
            scoreDisplayatGameOver.GetComponent<Text>().text = "0" + 0.ToString();
        }
        else
        {
            if (score < 10)
            {
                scoreDisplayatGameOver.GetComponent<Text>().text = "0" + score;
            }
            else
            {
                scoreDisplayatGameOver.GetComponent<Text>().text = score.ToString();
            }
        }
    }

    public void Paused()
    {
        if (!isPaused)
        {
            pauseSound.Play();
            isPaused = true;
            pauseButton.SetActive(false);
            pauseImage.SetActive(false);
            playButton.SetActive(true);
            playImage.SetActive(true);
            pausePanel.SetActive(true);
            //pausePanel.GetComponent<Animation>().Play("PausePanel");
            Time.timeScale = 0;
            levelMusic.Pause();
            //Player.Instance.jumpForce = 0.0f;
            //Rotator.Instance.speed = 0.0f;
        }

        else
        {
            pauseSound.Play();
            levelMusic.UnPause();
            isPaused = false;
            playButton.SetActive(false);
            playImage.SetActive(false);
            pauseButton.SetActive(true);
            pauseImage.SetActive(true);
            pausePanel.SetActive(false);
            Time.timeScale = 1;
            //Player.Instance.jumpForce = 10f;
            //Rotator.Instance.speed = 100f;
        }
    }

    public void MainMenuPausePanel()
    {
        isPaused = false;
        InterstitialAds.Instance.ShowInterstitial();
        clickSound.Play();
        Time.timeScale = 1;
        SceneManager.LoadScene("Main Menu");
    }

    public void MainMenuGameOverPanel()
    {
        isPaused = false;
        InterstitialAds.Instance.ShowInterstitial();
        StartCoroutine(Mainmenu());
        IEnumerator Mainmenu()
        {
            yield return new WaitForSeconds(0.5f);
            clickSound.Play();
            Time.timeScale = 1;
            SceneManager.LoadScene("Main Menu");
        }
    }

    public void Continue01GemButton()
    {
        Time.timeScale = 1;
        clickSound.Play();
        Player.Instance.gameOver = false;
        if (currentGem >= 1)
        {
            gemScore -= 1;
            player.SetActive(true);
            levelMusic.UnPause();
            pauseButton.SetActive(true);
            gameOverPanel.SetActive(false);
        }
        else
        {
            SceneManager.LoadScene("Shop");
        }
    }

    public void Restart()
    {
        Time.timeScale = 1;
        InterstitialAds.Instance.ShowInterstitial();
        StartCoroutine(restart());
        IEnumerator restart()
        {
            yield return new WaitForSeconds(0.5f);
            clickSound.Play();
            SceneManager.LoadScene("Level");
        }
    }

    public void ShareScore()
    {
        clickSound.Play();

        scoreDisplayAtSharePanel.text = scoreDisplay.GetComponent<Text>().text;
        DateTime dt = DateTime.Now;

        dateText.text = string.Format("{0}/{1}/{2}", dt.Month, dt.Day, dt.Year);

        scoreSharePanel.SetActive(true);
        StartCoroutine(TakeScreenshotAndShare());
        //StartCoroutine(LoadImageAndShare());

        IEnumerator TakeScreenshotAndShare()
        {
            yield return new WaitForEndOfFrame();

            Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            ss.Apply();

            string filePath = Path.Combine(Application.temporaryCachePath, "Card Switch.png");
            File.WriteAllBytes(filePath, ss.EncodeToPNG());

            Destroy(ss);

            new NativeShare().AddFile(filePath).SetSubject("Card Switch").SetText("Can you beat my score in Card Switch? Give it a try!!").Share();

            scoreSharePanel.SetActive(false);
        }

        //IEnumerator LoadImageAndShare()
        //{
        //    Texture2D image = Resources.Load("image", typeof(Texture2D)) as Texture2D;

        //    yield return null;

        //    string filePath = Path.Combine(Application.temporaryCachePath, "RainbowSnake.png");
        //    File.WriteAllBytes(filePath, image.EncodeToPNG());

        //    new NativeShare().AddFile(filePath).SetSubject("Rainbow Snake").SetText("Can you beat my score in Card Switch? Give it a try!!").Share();
        //}
    }

    public void PlayerCollidewithAnotherCard()
    {
        StartCoroutine(GameisOver());
        IEnumerator GameisOver()
        {
            levelMusic.Pause();
            //notSameColorCollide.Play();
            pauseButton.SetActive(false);
            player.SetActive(false);
            yield return new WaitForSeconds(1);
            gameOverPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void Leaderboard()
    {
        clickSound.Play();

        if (PlayerPrefs.GetInt("NameWindow", 0) <= 0)
        {
            nameWindow.gameObject.SetActive(true);
        }
        PlayerPrefs.SetInt("NameWindow", 1);

        leaderboardPanel.gameObject.SetActive(true);
    }

    public void LeaderboardBackArrowButton()
    {
        clickSound.Play();
        leaderboardPanel.SetActive(false);
        nameWindow.gameObject.SetActive(false);
    }

    void PlayfabManagement()
    {
        PlayfabManager.instance.Login();
    }

    public void UpdateScorePlayfab()
    {
        PlayfabManager.instance.SendLeaderboard(score);
    }
}
