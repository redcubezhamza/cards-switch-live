using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    // [SerializeField] Vector3 destroyBounds;
    [SerializeField] int offset;

    private GameObject referenceGameobject;


    // Start is called before the first frame update
    void Start()
    {
       referenceGameobject = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if ( transform.position.z < referenceGameobject.transform.position.z - offset && referenceGameobject != null )
        {
            Destroy(gameObject, 0.5f);
            Debug.Log("<color>Object Destroyed Out of Bounds </color>" + gameObject.name);
        }

    }
}
