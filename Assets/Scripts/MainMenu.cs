using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource clickSound;
    public AudioSource BGM;

    public int bestScore;

    public int gemScore;
    public GameObject gemScoreDisplay;

    void Start()
    {
        FreeGems();
        GemScore();
        BestScore();
    }

    public void FreeGems()
    {
        if (!PlayerPrefs.HasKey("init"))
        {
            PlayerPrefs.SetInt("init", 1);
            PlayerPrefs.SetInt("Gems", 50);
        }
    }

    public void GemScore()
    {
        gemScore = PlayerPrefs.GetInt("Gems");
        gemScoreDisplay.GetComponent<Text>().text = gemScore.ToString();

        if (gemScore < 0)
        {
            gemScoreDisplay.GetComponent<Text>().text = "0" + 0.ToString();
        }
        else
        {
            if (gemScore < 10)
            {
                gemScoreDisplay.GetComponent<Text>().text = "0" + gemScore;
            }
            else
            {
                gemScoreDisplay.GetComponent<Text>().text = gemScore.ToString();
            }
        }
    }

    public void BestScore()
    {
        bestScore = PlayerPrefs.GetInt("Level Score");
        //bestScoreDisplay.GetComponent<Text>().text = "" + bestScore;
    }

    public void PlayGame()
    {
        clickSound.Play();
        SceneManager.LoadScene("Level");
    }

    public void ExitGame()
    {
        clickSound.Play();
        /*EditorApplication.ExitPlaymode();*/
        Application.Quit();
    }

    public void Shop()
    {
        clickSound.Play();
        SceneManager.LoadScene("Shop");
    }
}
