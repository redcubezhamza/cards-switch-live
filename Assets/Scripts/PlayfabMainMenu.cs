using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class PlayfabMainMenu : MonoBehaviour
{
    public GameObject nameWindow;
    public InputField nameInput;

    public static PlayfabMainMenu instance;
    void Awake()
    {
        PlayFabSettings.TitleId = "2C40A"; //your title id goes here.
        instance = this;
    }
    public void SubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = nameInput.text,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
    }

    void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated Display Name");
        nameWindow.SetActive(false);
        //leaderboardWindow.SetActive(true);
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());
    }
}
