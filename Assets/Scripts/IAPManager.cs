using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour
{
    private string gem05 = "com.redcubez.shapeswitch.05gems";
    private string removeads = "com.redcubez.shapeswitch.removeads";

    public GameObject restoreButton;

    private void Awake()
    {
        if (Application.platform != RuntimePlatform.IPhonePlayer)
        {
            restoreButton.SetActive(false);
        }
    }

    public void OnPurchaseComplete(Product product)
    {
        if (product.definition.id == gem05)
        {
            Debug.Log("You have gained 05 Gems");
            int currentGem = PlayerPrefs.GetInt("Gems");
            int currentGem1 = currentGem + 5;
            PlayerPrefs.SetInt("Gems", currentGem1);
        }

        if (product.definition.id == removeads)
        {
            Debug.Log("All ads removed!");
        }
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(product.definition.id + "failed because" + failureReason);
    }
}
