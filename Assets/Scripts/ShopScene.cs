using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopScene : MonoBehaviour
{
    public AudioSource clickSound;
    public void MainMenuButton()
    {
        clickSound.Play();
        SceneManager.LoadScene("Main Menu");
    }

    public void RewardedAd()
    {
        RewardedAds.Instance.RequestRewarded();
        RewardedAds.Instance.ShowRewarded();

        int currentGem = PlayerPrefs.GetInt("Gems");
        int currentGem1 = currentGem + 1;
        PlayerPrefs.SetInt("Gems", currentGem1);
    }
}