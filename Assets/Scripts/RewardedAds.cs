using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class RewardedAds : MonoBehaviour
{
    public RewardedAd rewarded;

    public static RewardedAds Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { });
    }

    public AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void RequestRewarded()
    {
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
        if (this.rewarded != null)
            this.rewarded.Destroy();

        this.rewarded = new RewardedAd(adUnitId);
        this.rewarded.LoadAd(this.CreateAdRequest());
    }

    public void ShowRewarded()
    {
        if (this.rewarded.IsLoaded())
        {
            rewarded.Show();
        }
        else
        {
            Debug.Log("Rewarded is not ready yet");
        }
    }
}

