using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class SpawnManager : MonoBehaviour
{
    public GameObject[] floorPrefabs;
    public float zSpawn = 0;
    public float floorLength = 100;

    public int numberOfTiles = 1;
    public Transform player;
    private List<GameObject> activeFloor = new List<GameObject>();

    public GameObject[] cardsPool;

    [SerializeField] GameObject cardsToSpawnParent;
    [SerializeField] GameObject[] spawnedCards = new GameObject[4];
    [SerializeField] float[] cardSpawnXPos = new float[4];
    [SerializeField] int playerNextCard;
    [SerializeField] int forwardSpawnOffset = 15;


    private int randomShapes, temp;

    bool result;

    void Start()
    {
       // Player.Instance.nextCardMaterial = cardsPool[UnityEngine.Random.Range(0, cardsPool.Length)].gameObject.GetComponent<MeshRenderer>().sharedMaterial;
       // Player.Instance.currentMaterial = Player.Instance.nextCardMaterial.name;

        Debug.Log("Initial Material Assigned");


        for (int i = 0; i < numberOfTiles; i++)
        {
            if (i == 0)
                SpawnFloor(0);
            else
                SpawnFloor(0);
        }

        SpawningofShapes();
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            if (player.position.z - 50 > zSpawn - (numberOfTiles * floorLength))
            {
                SpawnFloor(0);
                DeleteFloor();
            }
        }
    }

    public void SpawnFloor(int floorIndex)
    {
        GameObject GO = Instantiate(floorPrefabs[floorIndex], transform.forward * zSpawn, transform.rotation);
        activeFloor.Add(GO);
        zSpawn += floorLength;
    }

    void DeleteFloor()
    {
        Destroy(activeFloor[0]);
        activeFloor.RemoveAt(0);
    }

    void SpawningofShapes()
    {
        InvokeRepeating("SpawnShapes", 1, 3);
    }

    void SpawnShapes()
    { // multiple life with spawn, even odd, time dependency off set speed instead/player cross 1 shape then must have shape`s shape. 
        if (Player.Instance.gameOver == false)
        {
            //randomShapes = Random.Range(0, shapesPrefabs.Length);
            Vector3 spawnPos0 = new Vector3(cardSpawnXPos[0], -1.4f, player.transform.position.z + forwardSpawnOffset);
            Vector3 spawnPos1 = new Vector3(cardSpawnXPos[1], -1.4f, player.transform.position.z + forwardSpawnOffset);
            Vector3 spawnPos2 = new Vector3(cardSpawnXPos[2], -1.4f, player.transform.position.z + forwardSpawnOffset);
            Vector3 spawnPos3 = new Vector3(cardSpawnXPos[3], -1.4f, player.transform.position.z + forwardSpawnOffset);






            //Instantiate(shapesPrefabs[randomShapes], spawnPos, shapesPrefabs[randomShapes].transform.rotation);

            //if (randomShapes == 0)
            //{
            //    Player.Instance.mr.material = Player.Instance.heart01;
            //}

            //if (randomShapes == 1)
            //{
            //    Player.Instance.mr.material = Player.Instance.heart02;
            //}

            //if (randomShapes == 2)
            //{
            //    Player.Instance.mr.material = Player.Instance.heart05;
            //}

            //if (randomShapes == 3)
            //{
            //    Player.Instance.mr.material = Player.Instance.king;
            //}

            //if (randomShapes == 4)
            //{
            //    Player.Instance.mr.material = Player.Instance.jack;
            //}

            //if (randomShapes == 5)
            //{
            //    Player.Instance.mr.material = Player.Instance.heart09;
            //}

            //if (randomShapes == 6)
            //{
            //    Player.Instance.mr.material = Player.Instance.heart03;
            //}

            //if (Player.Instance.currentMaterial == "Heart01")
            //    Instantiate(cardsPool[0], spawnPos, cardsPool[0].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart02")
            //    Instantiate(cardsPool[1], spawnPos, cardsPool[1].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart03")
            //    Instantiate(cardsPool[6], spawnPos, cardsPool[6].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart04")
            //    Instantiate(cardsPool[4], spawnPos, cardsPool[4].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart05")
            //    Instantiate(cardsPool[2], spawnPos, cardsPool[2].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart06")
            //    Instantiate(cardsPool[2], spawnPos, cardsPool[2].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart07")
            //    Instantiate(cardsPool[5], spawnPos, cardsPool[5].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart08")
            //    Instantiate(cardsPool[0], spawnPos, cardsPool[0].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart09")
            //    Instantiate(cardsPool[5], spawnPos, cardsPool[5].transform.rotation);
            //if (Player.Instance.currentMaterial == "Heart10")
            //    Instantiate(cardsPool[3], spawnPos, cardsPool[3].transform.rotation);
            //if (Player.Instance.currentMaterial == "King")
            //    Instantiate(cardsPool[3], spawnPos, cardsPool[3].transform.rotation);
            //if (Player.Instance.currentMaterial == "Queen")
            //    Instantiate(cardsPool[6], spawnPos, cardsPool[6].transform.rotation);
            //if (Player.Instance.currentMaterial == "Jack")
            //    Instantiate(cardsPool[4], spawnPos, cardsPool[4].transform.rotation);



            //List card[4] = new List[];




            result = int.TryParse(Player.Instance.currentMaterial, out temp);
           // int a = Convert.ToInt32(Player.Instance.currentMaterial);

            if (result)
            {
                int temp = int.Parse(Player.Instance.currentMaterial);

                    Debug.Log("<color=green> int pasrsed is </color>" + temp.ToString());
            }
            else
            {
                Debug.Log("<color=red> Unable to parse Player Material </color>");

                temp = 2;
            }
            

            

            //card[0] = cards[]

            spawnedCards[0] = cardsPool[temp-2];
            spawnedCards[1] = cardsPool[UnityEngine.Random.Range(0, cardsPool.Length)];
            spawnedCards[2] = cardsPool[UnityEngine.Random.Range(0, cardsPool.Length)];
            spawnedCards[3] = cardsPool[UnityEngine.Random.Range(0, cardsPool.Length)];





            Instantiate(spawnedCards[0], spawnPos0, transform.rotation, cardsToSpawnParent.transform);
            Instantiate(spawnedCards[1], spawnPos1, transform.rotation, cardsToSpawnParent.transform);
            Instantiate(spawnedCards[2], spawnPos2, transform.rotation, cardsToSpawnParent.transform);
            Instantiate(spawnedCards[3], spawnPos3, transform.rotation, cardsToSpawnParent.transform);


            playerNextCard = UnityEngine.Random.Range(0, 3);
            Player.Instance.nextCardMaterial = spawnedCards[playerNextCard].GetComponent<MeshRenderer>().sharedMaterial;
            
        }
    }
}
