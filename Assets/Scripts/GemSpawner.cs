using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemSpawner : MonoBehaviour
{
    public GameObject gemPrefab;
    public GameObject player;

    private float spawnRangeX = 1.5f;
    private float spawnRangeZ = 1.5f;

    void Start()
    {
        OnlyGemsSpawning();
    }

    void OnlyGemsSpawning()
    {
        InvokeRepeating("SpawnGem", Random.Range(20, 40), Random.Range(20, 80));
        //InvokeRepeating("SpawnGem", 2, 2);
    }

    void SpawnGem()
    {
        if (Player.Instance.gameOver == false)
        {
            Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), -1.733f, player.transform.position.z + 20);
            Instantiate(gemPrefab, spawnPos, transform.rotation);
        }
    }
}
